﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Validation;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using colinboyle.Models;

namespace colinboyle.Controllers
{
    public class BlogController : Controller
    {
        private BlogDataContext db = new BlogDataContext();

        // GET: Blog/All/1
        public ActionResult All(int? id)
        {
            if (id.HasValue) {
                int id2 = id.Value;
                if (id2 <= 0)
                {
                    return RedirectToAction("index", "home");
                }
                else
                {
                    return View(db.BlogPosts.Where(p => p.Status).OrderByDescending(p => p.CreatedDate).Skip(id2 * 5).Take(5).ToList());
                }
                
            }
            return View();
        }

        // GET: Blog/Search/searchString
        public ActionResult Search(string searchString, int? id)
        {
            if (!string.IsNullOrEmpty(searchString))
            {
                if (id == null || id <= 0) { id = 1; }
                int id2 = id.Value - 1;
                IEnumerable<BlogPost> blogPosts = db.BlogPosts.Where(p => p.Title.ToLower().Contains(searchString.ToLower())).OrderByDescending(p => p.CreatedDate).Skip(id2 * 5).Take(5).ToList();
                return View("All", blogPosts);
            }
            else
            {
                return RedirectToAction("index", "home");
            }
        }

        // GET: Blog/Article/5
        public ActionResult Article(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            //Selecting the article/
            BlogPost blogPost = db.BlogPosts.Find(id);

            //Selecting the previous and next article and saving them to the post.PrevPosts and post.NextPost//
            BlogPost PrevPost = db.BlogPosts.Where(x => x.Status == true).Where(x => !string.IsNullOrEmpty(x.Title)).Where(x => x.bpId < id).OrderByDescending(p => p.bpId).FirstOrDefault();
            BlogPost NextPost = db.BlogPosts.Where(x => x.Status == true).Where(x => !string.IsNullOrEmpty(x.Title)).Where(x => x.bpId > id).OrderBy(x => x.bpId).FirstOrDefault();

            //Selcting comments of the article//
            IEnumerable<BlogComment> comments = db.BlogComments.Where(c => c.BlogId == id && c.Status).ToList();

            //The tags returned for the specific article//
            IEnumerable<int> tagIds = db.PostTags.Where(p => p.PostId == id).Select(d => d.TagId).ToList();
            IEnumerable<BlogTag> tags = db.BlogTags.Where(t => tagIds.Contains(t.btId)).ToList();
           
            //The Post model holds the BlogPost, its tags, its comments, and the previous and next article//
            Post post = new Post();
            post.BlogPost = blogPost;
            post.BlogTags = tags;
            post.PrevPost = PrevPost;
            post.NextPost = NextPost;
            post.BlogComments = comments;
            if (blogPost == null)
            {
                return HttpNotFound();
            }
            return View(post);
        }

        // POST: blog/comment (comment)
        [HttpPost]
        [Authorize]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public ActionResult Comment([Bind(Include = "BlogId,UserId,Comment")] BlogComment blogComment)
        {
            if (ModelState.IsValid)
            {
                blogComment.DateCreated = DateTime.Now;
                db.BlogComments.Add(blogComment);
                var post = db.BlogPosts.First(p => p.bpId == blogComment.BlogId);
                post.Comments = post.Comments ++;
                db.SaveChanges();
                return RedirectToAction("article", "blog", new { id = blogComment.BlogId });
            }
            return RedirectToAction("index", "home");
        }

        // GET: blog/tag/tech
        public ActionResult Tag(string tag, int? id)
        {
            if (string.IsNullOrEmpty(tag))
            {
                return RedirectToAction("index", "home");
            }
            else if (id == null || id <= 0)
            {
                id = 1;
            }
            int id2 = id.Value - 1;
            int blogTag = db.BlogTags.First(t => t.Tag_Name == tag).btId;
            IList<int> postIds = db.PostTags.Where(t => t.TagId == blogTag).Select(d => d.PostId).ToList();
            IList<BlogPost> blogPosts = db.BlogPosts.Where(p => postIds.Contains(p.bpId)).OrderByDescending(p => p.CreatedDate).Skip(id2 * 5).Take(5).ToList();

            return View("all", blogPosts);
        }

        // GET: Blog/Create
        [Authorize(Users = "cb@cb.com")]
        public ActionResult Create()
        {
            return View();
        }

        // POST: Blog/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [Authorize(Users = "cb@cb.com")]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public ActionResult Create([Bind(Include = "Title,Picture,Summary,Post,Status,Featured")] BlogPost blogPost, [Bind(Include = "Tag_Name")] BlogTag blogTag)
        {
            if (ModelState.IsValid)
            {
                blogPost.CreatedDate = DateTime.Now;
                db.BlogPosts.Add(blogPost);
                db.SaveChanges();
                IEnumerable<string> blogTags = blogTag.Tag_Name.Split(',').ToList();
                foreach (string tag in blogTags)
                {
                    if (db.BlogTags.Any(t => t.Tag_Name.ToLower() == tag.ToLower()))
                    {
                        BlogTag oldTag = db.BlogTags.First(t => t.Tag_Name.ToLower() == tag.ToLower());
                        oldTag.Tag_Frequency = oldTag.Tag_Frequency + 1;
                    }
                    else
                    {
                        BlogTag newTag = new BlogTag();
                        newTag.Tag_Name = tag;
                        newTag.Tag_Frequency = 1;
                        db.BlogTags.Add(newTag);
                        db.SaveChanges();
                    }
                    PostTag postTag = new PostTag { PostId = blogPost.bpId, TagId = db.BlogTags.First(t => t.Tag_Name == tag).btId };
                    db.PostTags.Add(postTag);
                }
                db.SaveChanges();
                return RedirectToAction("all");
            }

            return View(blogPost);
        }

        // GET: Blog/Edit/5
        [Authorize(Users = "cb@cb.com")]
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            BlogPost blogPost = db.BlogPosts.Find(id);
            if (blogPost == null)
            {
                return HttpNotFound();
            }
            return View(blogPost);
        }

        // POST: Blog/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [Authorize(Users = "cb@cb.com")]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public ActionResult Edit([Bind(Include = "bpId,Title,CreatedDate,Image,Summary,Post,Status,Featured")] BlogPost blogPost)
        {
            if (ModelState.IsValid)
            {
                db.Entry(blogPost).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("all");
            }
            return View(blogPost);
        }

        // GET: Blog/Delete/5
        [Authorize(Users = "cb@cb.com")]
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            BlogPost blogPost = db.BlogPosts.Find(id);
            if (blogPost == null)
            {
                return HttpNotFound();
            }
            return View(blogPost);
        }

        // POST: Blog/Delete/5
        [HttpPost, ActionName("Delete")]
        [Authorize(Users = "cb@cb.com")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            BlogPost blogPost = db.BlogPosts.Find(id);
            db.BlogPosts.Remove(blogPost);
            db.SaveChanges();
            return RedirectToAction("all");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
