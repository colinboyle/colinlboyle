﻿using colinboyle.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace colinboyle.Controllers
{
    public class GalleryController : Controller
    {

        GalleryDataContext context = new GalleryDataContext();

        // GET: Images
        public ActionResult All()
        {
            return View(context.Images.ToList());
        }

        [Authorize(Users = "cb@cb.com")]
        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [Authorize(Users = "cb@cb.com")]
        public ActionResult Create(Image image)
        {
            if (!ModelState.IsValid) return View(image);
            context.Images.Add(image);
            context.SaveChanges();
            return RedirectToAction("All");
        }
    }
}