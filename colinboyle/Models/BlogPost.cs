namespace colinboyle.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("BlogPost")]
    public partial class BlogPost
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int bpId { get; set; }

        [Required]
        [StringLength(50)]
        public string Title { get; set; }

        [DisplayFormat(DataFormatString = "{0:dd MMM yyyy}")]
        public DateTime CreatedDate { get; set; }

        [StringLength(50)]
        public string Image { get; set; }

        [StringLength(160)]
        public string Summary { get; set; }

        public string Post { get; set; }

        public int Views { get; set; }

        public int Comments { get; set; }

        public int Likes { get; set; }

        public bool Status { get; set; }

        public bool Featured { get; set; }
    }
}
