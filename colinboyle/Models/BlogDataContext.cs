﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using WebMatrix.Data;

namespace colinboyle.Models
{
    public class BlogDataContext : DbContext
    {
        public BlogDataContext() : base("name=Blog")
        {

        }
        public DbSet<BlogPost> BlogPosts { get; set; }
        public DbSet<BlogTag> BlogTags { get; set; }
        public DbSet<PostTag> PostTags { get; set; }
        public DbSet<BlogComment> BlogComments { get; set; }
    }
}