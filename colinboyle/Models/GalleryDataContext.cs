﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace colinboyle.Models
{
    public class GalleryDataContext : DbContext
    {
        public GalleryDataContext() : base("name=Blog")
        {

        }
        public DbSet<Image> Images { get; set; }
    }
}