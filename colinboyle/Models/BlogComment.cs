namespace colinboyle.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("BlogComment")]
    public partial class BlogComment
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int bcId { get; set; }

        [Required]
        [StringLength(256)]
        public string UserId { get; set; }

        public int BlogId { get; set; }

        [Column(TypeName = "datetime2")]
        [DisplayFormat(DataFormatString = "{0:dd MMM yyyy}")]
        public DateTime DateCreated { get; set; }

        [Required]
        [StringLength(240)]
        public string Comment { get; set; }

        public bool Status { get; set; }

        public int? ReplyId { get; set; }
    }
}
