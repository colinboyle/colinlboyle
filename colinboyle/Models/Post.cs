﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace colinboyle.Models
{
    public class Post
    {
        public BlogPost BlogPost{ get; set; }
        public BlogPost PrevPost { get; set; }
        public BlogPost NextPost { get; set; }
        public BlogTag BlogTag { get; set; }
        public IEnumerable<BlogTag> BlogTags { get; set; }
        public BlogComment BlogComment { get; set; }
        public IEnumerable<BlogComment> BlogComments { get; set; }
    }
}