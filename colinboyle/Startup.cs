﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.Owin;
using Owin;
using System;
using System.Data.Entity.Migrations;
using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;
using colinboyle.Models;

[assembly: OwinStartupAttribute(typeof(colinboyle.Startup))]
namespace colinboyle
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
            //await CreateRoles(serviceProvider);
        }
        
        
        //private async Task CreateRoles(IServiceProvider serviceProvider)
        //{
       //    var RoleManager = serviceProvider.GetRequiredService<RoleManager<IdentityRole>>();
       //    var UserManager = serviceProvider.GetRequiredService<UserManager<ApplicationUser>>();
       //    string[] roleNames = { "Admin" };
       //    IdentityResult roleResult;
       //    foreach (var roleName in roleNames)
       //    {
       //        var roleExsist = await RoleManager.RoleExistsAsync(roleName);
       //        if (!roleExsist)
       //        {
       //            roleResult = await RoleManager.CreateAsync(new IdentityRole(roleName));
       //        }
       //    }
       //}
    }
}
