﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace colinboyle
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
                name: "Tags",
                url: "blog/tag/{tag}/{id}",
                defaults: new { controller = "blog", action = "tag", id = 1 }
            );

            routes.MapRoute(
                name: "Search",
                url: "blog/search/{id}/{searchString}",
                defaults: new { controller = "blog", action = "search", id = 1 }
            );

            routes.MapRoute(
                name: "Blog",
                url: "blog/{action}/{id}",
                defaults: new { controller = "blog", action = "all", id = 1 }
            );

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
