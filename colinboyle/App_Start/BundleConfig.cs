﻿using System.Web;
using System.Web.Optimization;

namespace colinboyle
{
    public class BundleConfig
    {
        // For more information on bundling, visit https://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at https://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/bootstrap.js",
                      "~/Scripts/respond.js"));

            bundles.Add(new ScriptBundle("~/bundles/script").Include(
                       "~/Scripts/owl.carousel.min.js",
                       "~/Scripts/owl.support.modernizr.js",
                       "~/Scripts/jquery.magnific-popup.min.js",
                       "~/Scripts/jquery.flexslider-min.js",
                       "~/Scripts/instafeed.min.js",
                       "~/Scripts/imagesloaded.pkgd.min.js",
                       "~/Scripts/isotopes.pkgd.min.js",
                       "~/Scripts/twitterFetcher_min.js",
                       //"~/Scripts/tweetie.min.js",
                       //"~/Content/TimelineMax.min.js",
                       "~/Scripts/script.js"));

            bundles.Add(new StyleBundle("~/Content/vendors").Include(
                      "~/Content/bootstrap.css",
                      "~/Content/font-awesome.css",
                      "~/Content/owl.carousel.css",
                      "~/Content/magnific-popup.css"
                      //"~/Content/owl.theme.default.min.css",
                      ));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/site.css",
                      "~/Content/purple.css"));
        }
    }
}
